/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80015
Source Host           : localhost:3306
Source Database       : interndb

Target Server Type    : MYSQL
Target Server Version : 80015
File Encoding         : 65001

Date: 2020-03-02 11:29:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for int_user
-- ----------------------------
DROP TABLE IF EXISTS `int_user`;
CREATE TABLE `int_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of int_user
-- ----------------------------
INSERT INTO `int_user` VALUES ('1', '小李', '0', '25', '1');
INSERT INTO `int_user` VALUES ('2', '小林', '1', '23', '2');
INSERT INTO `int_user` VALUES ('3', '小李', '1', '25', '1');
INSERT INTO `int_user` VALUES ('6', '小林3', '1', '3', '2');
INSERT INTO `int_user` VALUES ('17', '小林14', '0', '131', '1');
INSERT INTO `int_user` VALUES ('33', '小林30', '0', '22', '2');
INSERT INTO `int_user` VALUES ('59', '小林56', '1', '56', '2');
INSERT INTO `int_user` VALUES ('61', '小林58', '1', '58', '2');
INSERT INTO `int_user` VALUES ('62', '小林59', '1', '59', '2');
INSERT INTO `int_user` VALUES ('69', '小林66', '0', '113', '2');
INSERT INTO `int_user` VALUES ('70', '小林67', '0', '67', '2');
INSERT INTO `int_user` VALUES ('71', '小林68', '0', '68', '2');
INSERT INTO `int_user` VALUES ('72', '小林69', '0', '69', '2');
INSERT INTO `int_user` VALUES ('73', '小林70', '0', '70', '2');
INSERT INTO `int_user` VALUES ('74', '小林71', '0', '71', '2');
INSERT INTO `int_user` VALUES ('75', '小林72', '0', '72', '2');
INSERT INTO `int_user` VALUES ('76', '小林73', '0', '73', '2');
INSERT INTO `int_user` VALUES ('77', '小林74', '0', '74', '2');
INSERT INTO `int_user` VALUES ('78', '小林75', '0', '75', '2');
INSERT INTO `int_user` VALUES ('79', '小林76', '0', '76', '2');
INSERT INTO `int_user` VALUES ('80', '小林77', '0', '77', '2');
INSERT INTO `int_user` VALUES ('82', '小林79', '0', '79', '2');
INSERT INTO `int_user` VALUES ('84', '小林81', '0', '81', '2');
INSERT INTO `int_user` VALUES ('85', '小林82', '0', '82', '2');
INSERT INTO `int_user` VALUES ('86', '小林83', '0', '83', '2');
INSERT INTO `int_user` VALUES ('87', '4124', '0', '312312', '1');
INSERT INTO `int_user` VALUES ('88', '小林85', '0', '85', '2');
INSERT INTO `int_user` VALUES ('89', '小林86', '0', '86', '2');
INSERT INTO `int_user` VALUES ('90', '小林87', '0', '87', '2');
INSERT INTO `int_user` VALUES ('93', '小林90', '0', '90', '2');
INSERT INTO `int_user` VALUES ('96', '小林93', '0', '93', '2');
INSERT INTO `int_user` VALUES ('97', '小林94', '0', '94', '2');
INSERT INTO `int_user` VALUES ('98', '小林95', '0', '95', '2');
INSERT INTO `int_user` VALUES ('99', '小林96', '0', '96', '2');
INSERT INTO `int_user` VALUES ('100', '小林97', '1', '97', '2');
INSERT INTO `int_user` VALUES ('101', '小林98', '0', '98', '2');
INSERT INTO `int_user` VALUES ('102', '小林99', '0', '99', '2');
INSERT INTO `int_user` VALUES ('103', '小林1', '0', '1', '2');
INSERT INTO `int_user` VALUES ('104', '小林2', '0', '2', '2');
INSERT INTO `int_user` VALUES ('105', '小林3', '0', '3', '2');
INSERT INTO `int_user` VALUES ('106', '小林4', '0', '4', '2');
INSERT INTO `int_user` VALUES ('107', '小林5', '0', '5', '2');
INSERT INTO `int_user` VALUES ('108', '小林6', '1', '6', '2');
INSERT INTO `int_user` VALUES ('109', '小林7', '1', '7', '2');
INSERT INTO `int_user` VALUES ('110', '小林8', '1', '8', '2');
INSERT INTO `int_user` VALUES ('111', '小林9', '1', '9', '2');
INSERT INTO `int_user` VALUES ('112', '小林10', '1', '10', '2');
INSERT INTO `int_user` VALUES ('113', '小林11', '1', '11', '2');
INSERT INTO `int_user` VALUES ('114', '小林12', '1', '12', '2');
INSERT INTO `int_user` VALUES ('115', '小林13', '1', '13', '2');
INSERT INTO `int_user` VALUES ('116', '小林14', '1', '14', '2');
INSERT INTO `int_user` VALUES ('117', '小林15', '1', '15', '2');
INSERT INTO `int_user` VALUES ('118', '小林16', '1', '16', '2');
INSERT INTO `int_user` VALUES ('119', '小林17', '1', '17', '2');
INSERT INTO `int_user` VALUES ('120', '小林18', '1', '18', '2');
INSERT INTO `int_user` VALUES ('121', '小林19', '1', '19', '2');
INSERT INTO `int_user` VALUES ('122', '小林20', '1', '20', '2');
INSERT INTO `int_user` VALUES ('123', '小林21', '1', '21', '2');
INSERT INTO `int_user` VALUES ('124', '小林22', '1', '22', '2');
INSERT INTO `int_user` VALUES ('125', '小林23', '1', '23', '2');
INSERT INTO `int_user` VALUES ('126', '小林24', '1', '24', '2');
INSERT INTO `int_user` VALUES ('127', '小林25', '1', '25', '2');
INSERT INTO `int_user` VALUES ('128', '小林26', '1', '26', '2');
INSERT INTO `int_user` VALUES ('129', '小林27', '1', '27', '2');
INSERT INTO `int_user` VALUES ('130', '小林28', '1', '28', '2');
INSERT INTO `int_user` VALUES ('131', '小林29', '1', '29', '2');
INSERT INTO `int_user` VALUES ('132', '小林30', '1', '30', '2');
INSERT INTO `int_user` VALUES ('133', '小林31', '1', '31', '2');
INSERT INTO `int_user` VALUES ('134', '小林32', '1', '32', '2');
INSERT INTO `int_user` VALUES ('135', '小林33', '1', '33', '2');
INSERT INTO `int_user` VALUES ('136', '小林34', '1', '34', '2');
INSERT INTO `int_user` VALUES ('137', '小林35', '1', '35', '2');
INSERT INTO `int_user` VALUES ('138', '小林36', '1', '36', '2');
INSERT INTO `int_user` VALUES ('139', '小林37', '1', '37', '2');
INSERT INTO `int_user` VALUES ('140', '小林38', '1', '38', '2');
INSERT INTO `int_user` VALUES ('141', '小林39', '1', '39', '2');
INSERT INTO `int_user` VALUES ('142', '小林40', '1', '40', '2');
INSERT INTO `int_user` VALUES ('143', '小林41', '1', '41', '2');
INSERT INTO `int_user` VALUES ('144', '小林42', '1', '42', '2');
INSERT INTO `int_user` VALUES ('145', '小林43', '0', '43', '2');
INSERT INTO `int_user` VALUES ('146', '小林44', '0', '44', '2');
INSERT INTO `int_user` VALUES ('147', '小林45', '0', '45', '2');
INSERT INTO `int_user` VALUES ('148', '小林46', '0', '46', '2');
INSERT INTO `int_user` VALUES ('149', '小林47', '0', '47', '2');
INSERT INTO `int_user` VALUES ('150', '小林48', '0', '48', '2');
INSERT INTO `int_user` VALUES ('151', '小林49', '0', '49', '2');
INSERT INTO `int_user` VALUES ('152', '小林50', '0', '50', '2');
INSERT INTO `int_user` VALUES ('153', '小林51', '0', '51', '2');
INSERT INTO `int_user` VALUES ('154', '小林52', '1', '52', '2');
INSERT INTO `int_user` VALUES ('155', '小林53', '1', '53', '2');
INSERT INTO `int_user` VALUES ('156', '小林54', '1', '54', '2');
INSERT INTO `int_user` VALUES ('157', '小林55', '1', '55', '2');
INSERT INTO `int_user` VALUES ('158', '小林56', '1', '56', '2');
INSERT INTO `int_user` VALUES ('159', '小林57', '1', '57', '2');
INSERT INTO `int_user` VALUES ('160', '小林58', '1', '58', '2');
INSERT INTO `int_user` VALUES ('161', '小林59', '1', '59', '2');
INSERT INTO `int_user` VALUES ('162', '小林60', '1', '60', '1');
INSERT INTO `int_user` VALUES ('163', '小林61', '1', '61', '1');
INSERT INTO `int_user` VALUES ('164', '小林62', '1', '62', '1');
INSERT INTO `int_user` VALUES ('165', '小林63', '1', '63', '1');
INSERT INTO `int_user` VALUES ('166', '小林64', '1', '64', '1');
INSERT INTO `int_user` VALUES ('167', '小林65', '1', '65', '1');
INSERT INTO `int_user` VALUES ('168', '小林66', '1', '66', '1');
INSERT INTO `int_user` VALUES ('169', '小林67', '1', '67', '1');
INSERT INTO `int_user` VALUES ('170', '小林68', '1', '68', '1');
INSERT INTO `int_user` VALUES ('171', '小林69', '1', '69', '1');
INSERT INTO `int_user` VALUES ('172', '小林70', '1', '70', '1');
INSERT INTO `int_user` VALUES ('173', '小林71', '1', '71', '1');
INSERT INTO `int_user` VALUES ('174', '小林72', '1', '72', '1');
INSERT INTO `int_user` VALUES ('175', '小林73', '0', '73', '1');
INSERT INTO `int_user` VALUES ('176', '小林74', '0', '74', '1');
INSERT INTO `int_user` VALUES ('177', '小林75', '0', '75', '1');
INSERT INTO `int_user` VALUES ('178', '小林76', '0', '76', '1');
INSERT INTO `int_user` VALUES ('179', '小林77', '0', '77', '1');
INSERT INTO `int_user` VALUES ('180', '小林78', '0', '78', '1');
INSERT INTO `int_user` VALUES ('181', '小林79', '0', '79', '1');
INSERT INTO `int_user` VALUES ('182', '小林80', '0', '80', '1');
INSERT INTO `int_user` VALUES ('183', '小林81', '0', '81', '1');
INSERT INTO `int_user` VALUES ('184', '小林82', '0', '82', '1');
INSERT INTO `int_user` VALUES ('185', '小林83', '0', '83', '1');
INSERT INTO `int_user` VALUES ('186', '小林84', '0', '84', '1');
INSERT INTO `int_user` VALUES ('187', '小林85', '0', '85', '1');
INSERT INTO `int_user` VALUES ('188', '小林86', '0', '86', '1');
INSERT INTO `int_user` VALUES ('189', '小林87', '0', '87', '1');
INSERT INTO `int_user` VALUES ('190', '小林88', '0', '88', '1');
INSERT INTO `int_user` VALUES ('191', '小林89', '0', '89', '1');
INSERT INTO `int_user` VALUES ('192', '小林90', '1', '90', '1');
INSERT INTO `int_user` VALUES ('193', '小林91', '1', '91', '1');
INSERT INTO `int_user` VALUES ('194', '小林92', '1', '92', '2');
INSERT INTO `int_user` VALUES ('195', '小林93', '1', '93', '2');
INSERT INTO `int_user` VALUES ('196', '小林94', '1', '94', '2');
INSERT INTO `int_user` VALUES ('197', '小林95', '1', '95', '2');
INSERT INTO `int_user` VALUES ('198', '小林96', '1', '96', '2');
INSERT INTO `int_user` VALUES ('199', '小林97', '1', '97', '2');
INSERT INTO `int_user` VALUES ('200', '小林98', '1', '98', '2');
INSERT INTO `int_user` VALUES ('201', '小林99', '1', '99', '2');
INSERT INTO `int_user` VALUES ('202', null, '0', '0', '0');
INSERT INTO `int_user` VALUES ('203', null, '0', '0', '0');
INSERT INTO `int_user` VALUES ('204', '2', '0', '1', '1');
INSERT INTO `int_user` VALUES ('205', '31', '1', '4124', '2');
INSERT INTO `int_user` VALUES ('206', '13123', '0', '31231', '1');
INSERT INTO `int_user` VALUES ('207', '13123', '0', '31231', '1');
INSERT INTO `int_user` VALUES ('210', '312', '0', '11', '1');
INSERT INTO `int_user` VALUES ('211', '21', '0', '13', '1');
INSERT INTO `int_user` VALUES ('212', '21', '0', '13', '1');
INSERT INTO `int_user` VALUES ('213', '21', '0', '13', '1');
INSERT INTO `int_user` VALUES ('214', 'admin', '1', '11', '1');
INSERT INTO `int_user` VALUES ('216', '312', '0', '412', '2');
INSERT INTO `int_user` VALUES ('217', '31', '0', '1231', '1');
INSERT INTO `int_user` VALUES ('218', '3123', '1', '12', '1');
INSERT INTO `int_user` VALUES ('219', '12', '0', '1', '2');
INSERT INTO `int_user` VALUES ('220', '1121', '1', '131', '1');
