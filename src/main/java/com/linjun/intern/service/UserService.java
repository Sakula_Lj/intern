package com.linjun.intern.service;

import com.linjun.intern.model.User;


import java.util.List;

/**
 * @description:
 * @author: JUN MR.
 * @time: 2020/2/26 19:33
 */

public interface UserService {

    List<User> ListByUser();

   User UserById(int id);

    int addUser(User user);

    int updateUser(User user);

    int delUser(int id);

}
