package com.linjun.intern.service.impl;

import com.github.pagehelper.PageHelper;
import com.linjun.intern.dao.UserDao;
import com.linjun.intern.model.User;
import com.linjun.intern.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: JUN MR.
 * @time: 2020/2/26 19:34
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<User> ListByUser() {

        return userDao.ListByUser();
    }

    @Override
    public User UserById(int id) {
        return userDao.UserById(id);
    }

    @Override
    public int addUser(User user) {

        return userDao.addUser(user);
    }

    @Override
    public int updateUser(User user) {

        return userDao.updateUser(user);
    }

    @Override
    public int delUser(int id) {

        return userDao.delUser(id);
    }
}
