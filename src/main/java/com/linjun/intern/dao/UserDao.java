package com.linjun.intern.dao;

import com.linjun.intern.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @description:
 * @author: JUN MR.
 * @time: 2020/2/25 22:13
 */

@Mapper
public interface UserDao {


//    查询所有

    @Select("select * from int_user")
    List<User> ListByUser();


//      通过id查询所有

    @Select("select * from int_user where id=#{id}")
    User UserById(int id);

//    数据增加

    @Insert("insert into int_user(username,sex,age,position) values (#{username},#{sex},#{age},#{position})")
    int addUser(User user);


//    修改数据

    @Update("update int_user set username=#{username},sex=#{sex},age=#{age},position=#{position} where id=#{id}")
    int updateUser(User user);


//    删除数据

    @Delete("delete from int_user where id=#{id}")
    int delUser(int id);

}
