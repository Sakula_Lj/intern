package com.linjun.intern.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: JUN MR.
 * @time: 2020/2/25 22:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class User {

    private int id;
    private String username;
    private int sex;
    private int age;
    private int position;
}
