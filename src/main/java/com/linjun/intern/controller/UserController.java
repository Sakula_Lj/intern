package com.linjun.intern.controller;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.linjun.intern.model.User;
import com.linjun.intern.service.UserService;
import com.linjun.intern.util.Msg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @description: CRUD
 * @author: JUN MR.
 * @time: 2020/2/26 19:02
 */

@Slf4j
@Controller
public class UserController {


    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String listUser(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                           @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
    Model model){

//        用来设置页面的初始位置和展示的数据条目数；
        PageHelper.startPage(pageNum,10);
        List<User> userList= userService.ListByUser();
//        用来封装页面信息，返回给前台界面
//        分页后，实际返回的结果list类型是Page<E>，如果想取出分页信息，需要强制转换为Page<E>，
//        PageInfo<User> page=new PageInfo<>(userList);
        PageInfo page = new PageInfo(userList,5);
        model.addAttribute("pages",page);
//        model.addAttribute("uid","123456789");
        System.out.println(page);
//        model.addAttribute("name")

        return "index";
    }

    @RequestMapping(value = "/UserById/{id}" ,method = RequestMethod.GET)
    @ResponseBody
    public User delUser(@PathVariable("id") int id){
        log.info("查询用户id="+id);
        User i= userService.UserById(id);
        System.out.println(i);

        return i;
    }



    @RequestMapping(value = "/delUser/{id}" ,method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> delUser(@PathVariable("id") int id, ModelAndView mv){
        log.info("删除用户id="+id);
        int i= userService.delUser(id);
        System.out.println(i);
        Map<String,Object> resultMap = new HashMap<String, Object>();
        if (i==1) {
            resultMap.put("type", "success");
        }else{
            resultMap.put("type", "err");
        }
        return resultMap;
    }

    @RequestMapping(value = "/addUser" ,method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> addUser(User user){

        log.info("增加用户id="+user);
        int i= userService.addUser(user);
        System.out.println(i);
        Map<String,Object> resultMap = new HashMap<String, Object>();
        if (i==1) {
            resultMap.put("type", "success");
        }else{
            resultMap.put("type", "err");
        }
        return resultMap;

    }

    @RequestMapping(value = "/updateUser" ,method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> updateUser(User user){

        log.info("修改用户用户id="+user);
        int i= userService.updateUser(user);
        System.out.println(i);
        Map<String,Object> resultMap = new HashMap<String, Object>();
        if (i==1) {
            resultMap.put("type", "success");
        }else{
            resultMap.put("type", "err");
        }
        return resultMap;

    }
    @RequestMapping(value = "/listUserExc" ,method = RequestMethod.GET)
    @ResponseBody
    public List<User> listUserExc(){


        List list= userService.ListByUser();
        log.info("修改用户用户id="+list);
        return list;

    }

}
